import math
import numpy as np
import pandas as pd


def read_mut_vcf(pos_fn):
    cols = ['chrom', 'pos', 'id', 'ref', 'alt', 'qual', 'filter', 'info']
    vcf = pd.read_table(pos_fn, header=None, names=cols,
                        dtype={'chrom': str}, comment='#')
    return vcf


def read_sig(sig_fn):
    sig = pd.read_table(argv.sig)
    sig.set_index('Somatic Mutation Type', inplace=True)
    del sig['Substitution Type']
    del sig['Trinucleotide']
    return sig


def get_trinucleotide_context(pos):
    tc_idx = 0
    for info in pos.iloc[0]['info'].split(';'):
        if 'TC' == info[:2]:
            break
        else:
            tc_idx += 1
    tc = pos['info'].str.split(';').str[tc_idx].str.replace('TC=', '')
    return tc


def revcomp(seq):
    mapping = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    bases = list(seq)
    bases = [mapping[base] for base in bases]
    seq = ''.join(bases)[::-1]
    return seq


def normalize_bases(row):
    purine = ['A', 'G']
    if row['ref'] in purine:
        row['ref'] = revcomp(row['ref'])
        row['alt'] = revcomp(row['alt'])
        row['trinucleotide'] = revcomp(row['trinucleotide'])
    return row


def extract_mut_types(pos):
    pos['trinucleotide'] = get_trinucleotide_context(pos)
    pos = pos.apply(normalize_bases, axis=1)

    mut_types = []
    template = '{}[{}>{}]{}'
    for idx, row in pos.iterrows():
        tc = row['trinucleotide']
        mut_type = template.format(tc[0], tc[1], row['alt'], tc[2])
        mut_types.append(mut_type)
    return mut_types


def extract_mut_catalogue(pos):
    mut_types = extract_mut_types(pos)
    mut_types = pd.DataFrame({'mut_type': mut_types})
    mut_catalogue = mut_types.groupby('mut_type').size()
    return mut_catalogue


def normalize(l):
    l = [float(x) / sum(l) for x in l]
    return l


def calc_resp(x, pi, theta):
    r = np.zeros([len(pi), x.sum()])
    r_i = 0
    for j in range(len(x)):
        p = theta[j, :]
        p = p * pi
        p_sum = p.sum()
        p = p / p.sum()
        p = np.transpose(np.tile(p, (x[j], 1)))
        r_j = r_i + x[j]
        r[:, r_i:r_j] = p
        r_i = r_j
    return r


def calc_pi(r, N):
    pi = np.zeros(len(r))
    for k in range(len(r)):
        pi[k] = r[k].sum() / N
    return pi


def fit_pi(x, pi_old, theta):
    r = calc_resp(x, pi_old, theta)
    pi_new = calc_pi(r, x.sum())
    return pi_new


def calc_ll(x, pi, theta):
    r = calc_resp(x, pi, theta)
    ll = 0
    for k in range(len(pi)):
        i = 0
        for j in range(len(x)):
            ll += x[j] * r[k, i] * (np.log(pi[k]) + np.log(theta[j, k]))
            i += x[j]
    return ll


def fit_model(x, theta):
    pi = np.random.rand(theta.shape[1])
    pi /= pi.sum()

    ll_old = calc_ll(x, pi, theta)
    ll_rel_diff = np.inf
    while ll_rel_diff > 1e-3:
        pi = fit_pi(x, pi, theta)
        ll = calc_ll(x, pi, theta)
        ll_rel_diff = np.abs((ll - ll_old) / ll)
        ll_old = ll
    return pi, ll


def detect_sig(cat, sig, n_starts=10):
    cat = cat.reindex(sig.index.tolist(), fill_value=0)

    for col in sig.columns:
        sig[col] = normalize(sig[col] + 10e-10)

    cat_val = cat.values
    sig_val = sig.values

    pi = None
    ll = None
    for i in range(n_starts):
        pi_new, ll_new = fit_model(cat_val, sig_val)
        print('Start {} - LL: {}'.format(i + 1, ll_new))

        if ll is None or ll_new > ll:
            pi = pi_new
            ll = ll_new
    
    sig_pi = pd.Series(pi, sig.columns.tolist())
    return sig_pi, ll


def calc_bic(ll, N, deg_freedom):
    return (-2 * ll) + (math.log(N) * deg_freedom)


def rm_feat(sig_pi, sig):
    sig_pi.sort(ascending=False, inplace=True)

    print('Removing: {} and retrying...'.format(sig_pi.index[-1]))

    sig_keep = sig_pi.index.tolist()[:-1]
    sig = sig[sig_keep].copy()
    return sig


def rec_feat_elim(cat, sig, n_starts=10):
    '''Recursive feature elimination'''
    N = cat.sum()

    new_sig_pi, new_ll = detect_sig(cat, sig, n_starts)
    new_bic = calc_bic(new_ll, N, sig.shape[1] - 1)
    print('BIC: {}'.format(new_bic))

    bic = np.inf
    while new_bic < bic:
        sig_pi, ll, bic = new_sig_pi, new_ll, new_bic          

        sig = rm_feat(sig_pi, sig)
        new_sig_pi, new_ll = detect_sig(cat, sig, n_starts)
        new_bic = calc_bic(new_ll, N, sig.shape[1] - 1)
        print('BIC: {}'.format(new_bic))

    print('Selected {} signatures.'.format(sig.shape[1]))
    return sig_pi, bic


def reduce_mut_types(cat):
    cat_prop = cat / cat.sum()
    cat_prop.sort(inplace=True)
    rm_types = []
    prop_sum = 0
    for idx, prop in cat_prop.iteritems():
        if prop_sum + prop < 0.01:
            prop_sum += prop
            rm_types.append(idx)
        else:
            break

    cat = cat[~cat.index.isin(rm_types)]
    return cat, rm_types


if __name__ == '__main__':
    from argparse import ArgumentParser

    desc = 'Identify mutation signatures from MutationSeq calls'
    p = ArgumentParser(description=desc)

    p.add_argument('mut', help='MutationSeq result VCF file')
    p.add_argument('sig', help='mutation signature TSV file')
    p.add_argument('out', help='output TSV file')

    argv = p.parse_args()

    sig = read_sig(argv.sig)

    print('Extracting mutation types from VCF...')
    vcf = read_mut_vcf(argv.mut)
    cat = extract_mut_catalogue(vcf)

    print('Removing infrequent mutation types...')
    cat, rm_types = reduce_mut_types(cat)
    print('Removing these mutation types:\n{}'.format(rm_types))

    sig_pi, ll = detect_sig(cat, sig, 10)
    sig_pi.sort(ascending=False)
    print(ll)
    print(sig_pi)

    sig_pi, bic = rec_feat_elim(cat, sig, 3)
    sig_pi.sort(ascending=False)
    print(bic)
    print(sig_pi)
