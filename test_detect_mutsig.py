import unittest
import numpy as np
from numpy.testing import *
import math
import pandas as pd
import detect_mutsig


class DetectMutSigTest(unittest.TestCase):
    """Tests for detect_mutsig.py"""

    def test_get_trinucleotide_context(self):
        info = ['FOO=0;TC=AAA;BAR=A',
                'FOO=1;TC=GGG;BAR=B',
                'FOO=2;TC=ACA;BAR=C']
        df = pd.DataFrame({'info': info})

        tc = detect_mutsig.get_trinucleotide_context(df)
        self.assertSequenceEqual(tc.tolist(), ['AAA', 'GGG', 'ACA'])
        return

    def test_revcomp(self):
        rc = detect_mutsig.revcomp('ACG')
        self.assertEqual(rc, 'CGT')

        rc = detect_mutsig.revcomp('TTC')
        self.assertEqual(rc, 'GAA')
        return

    def test_normalize_bases(self):
        row = pd.Series({'ref': 'A', 'alt': 'G', 'trinucleotide': 'CAG'})
        norm = pd.Series({'ref': 'T', 'alt': 'C', 'trinucleotide': 'CTG'})
        res = detect_mutsig.normalize_bases(row)
        self.assertSequenceEqual(res.tolist(), norm.tolist())

        row = pd.Series({'ref': 'T', 'alt': 'C', 'trinucleotide': 'ATA'})
        norm = pd.Series({'ref': 'T', 'alt': 'C', 'trinucleotide': 'ATA'})
        res = detect_mutsig.normalize_bases(row)
        self.assertSequenceEqual(res.tolist(), norm.tolist())
        return

    def test_extract_mut_types(self):
        info = ['FOO=0;TC=ACA;BAR=A',
                'FOO=1;TC=TTT;BAR=B',
                'FOO=2;TC=CGC;BAR=C',
                'FOO=3;TC=CAT;BAR=D']
        ref = ['C', 'T', 'G', 'A']
        alt = ['A', 'C', 'T', 'G']
        df = pd.DataFrame({'ref': ref, 'alt': alt, 'info': info})

        mut_types = ['A[C>A]A', 'T[T>C]T', 'G[C>A]G', 'A[T>C]G']
        res = detect_mutsig.extract_mut_types(df)
        self.assertSequenceEqual(res, mut_types)
        return

    def test_extract_mut_catalogue(self):
        info = ['FOO=0;TC=ACA;BAR=A',
                'FOO=1;TC=TTT;BAR=B',
                'FOO=2;TC=CGC;BAR=C',
                'FOO=3;TC=CAT;BAR=D',
                'FOO=4;TC=TTT;BAR=E',
                'FOO=5;TC=CGC;BAR=F']
        ref = ['C', 'T', 'G', 'A', 'T', 'G']
        alt = ['A', 'C', 'T', 'G', 'C', 'T']
        df = pd.DataFrame({'ref': ref, 'alt': alt, 'info': info})

        mut_types = ['A[C>A]A', 'A[T>C]G', 'G[C>A]G', 'T[T>C]T']
        counts = [1, 1, 2, 2]

        res = detect_mutsig.extract_mut_catalogue(df)
        self.assertSequenceEqual(res.index.tolist(), mut_types)
        self.assertSequenceEqual(res.tolist(), counts)
        return

    def test_normalize(self):
        a = detect_mutsig.normalize([1, 4, 6, 7])
        self.assertAlmostEqual(sum(a), 1.0)
        return

    def test_calc_resp(self):
        cat = np.array([1, 2, 3])
        sig = np.array([[0.5, 0.2], [0.4, 0.3], [0.1, 0.5]])
        pi = np.array([0.3, 0.7])
        r = np.array([[0.51724138] + [0.36363636] * 2 + [0.07894737] * 3,
                     [0.48275862] + [0.63636364] * 2 + [0.92105263] * 3])
        res = detect_mutsig.calc_resp(cat, pi, sig)
        assert_array_almost_equal(res, r)
        return

    def test_calc_pi(self):
        pi = np.array([0.3, 0.7])
        r = np.array([[0.51724138] + [0.36363636] * 2 + [0.07894737] * 3,
                     [0.48275862] + [0.63636364] * 2 + [0.92105263] * 3])
        pi_new = np.array([r[0, :].sum() / 6, r[1, :].sum() / 6])
        res = detect_mutsig.calc_pi(r, 6)
        assert_array_almost_equal(res, pi_new)
        return

    def test_fit_pi(self):
        cat = np.array([1, 2, 3])
        sig = np.array([[0.5, 0.2], [0.4, 0.3], [0.1, 0.5]])
        pi = np.array([0.3, 0.7])
        r = np.array([[0.51724138] + [0.36363636] * 2 + [0.07894737] * 3,
                      [0.48275862] + [0.63636364] * 2 + [0.92105263] * 3])
        pi_new = np.array([r[0, :].sum() / 6, r[1, :].sum() / 6])
        res = detect_mutsig.fit_pi(cat, pi, sig)
        assert_array_almost_equal(res, pi_new)
        return

    def test_calc_ll(self):
        cat = np.array([1, 2, 3])
        sig = np.array([[0.5, 0.2], [0.4, 0.3], [0.1, 0.5]])
        pi = np.array([0.3, 0.7])
        ll = -9.1900405387343458
        res = detect_mutsig.calc_ll(cat, pi, sig)
        self.assertAlmostEqual(res, ll)
        return

    def test_fit_model(self):
        cat = np.array([1, 2, 3])
        sig = np.array([[0.5, 0.2], [0.4, 0.3], [0.1, 0.5]])
        pi, ll = detect_mutsig.fit_model(cat, sig)
        self.assertAlmostEqual(np.sum(pi), 1.0)
        self.assertLess(ll, 0)
        return

    def test_detect_sig(self):
        cat = pd.Series([1, 2, 3], ['A[T>C]G', 'G[C>A]G', 'T[T>C]T'])
        sig = pd.DataFrame({'Signature 1': [0.5, 0.4, 0.1],
                            'Signature 2': [0.2, 0.3, 0.5]},
                           index=cat.index)
        pi, ll = detect_mutsig.detect_sig(cat, sig)
        self.assertAlmostEqual(np.sum(pi), 1.0)
        self.assertLess(ll, 0)
        return

if __name__ == '__main__':
    unittest.main()
