import pandas as pd
import subprocess
from detect_mutsig import detect_sig
from ruffus import *  # NOQA


def top_signature_kld(cat, sig):
    kld_sigs = detect_sig(cat, sig, method='kld')
    top_sig = kld_sigs[kld_sigs['kld'] == kld_sigs['kld'].min()].iloc[0]
    return top_sig


@follows(mkdir('alexandrov_nature_2013/top_signatures'))
@transform('alexandrov_nature_2013/catalogues/*', regex(r'.+/(.+)_genomes.+'),
           add_inputs('signatures.txt'),
           r'alexandrov_nature_2013/top_signatures/\1_topsig_kld.tsv')
def topsig_kld(infiles, outfile):
    cat = pd.read_table(infiles[0])
    sig = pd.read_table(infiles[1])

    cat.set_index('Mutation Type', inplace=True)

    top_sigs = cat.apply(top_signature_kld, args=(sig, )).transpose()
    top_sigs.to_csv(outfile, sep='\t')
    return


def top_signature_ll(cat, sig):
    ll_sigs = detect_sig(cat, sig, method='ll')
    top_sig = ll_sigs[ll_sigs['ll'] == ll_sigs['ll'].max()].iloc[0]
    return top_sig


@follows(mkdir('alexandrov_nature_2013/top_signatures'))
@transform('alexandrov_nature_2013/catalogues/*', regex(r'.+/(.+)_genomes.+'),
           add_inputs('signatures.txt'),
           r'alexandrov_nature_2013/top_signatures/\1_topsig_ll.tsv')
def topsig_ll(infiles, outfile):
    cat = pd.read_table(infiles[0])
    sig = pd.read_table(infiles[1])

    cat.set_index('Mutation Type', inplace=True)

    top_sigs = cat.apply(top_signature_ll, args=(sig, )).transpose()
    top_sigs.to_csv(outfile, sep='\t')
    return


@transform([topsig_kld, topsig_ll], suffix('tsv'), 'pdf')
def topsig_hist(infile, outfile):
    script = './make_topsig_histogram.R'

    subprocess.call([script, infile, outfile])
    return


pipeline_run(checksum_level=0)
